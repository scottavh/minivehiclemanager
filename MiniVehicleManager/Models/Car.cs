﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;

namespace MiniVehicleManager.Models
{
    public class Car : Vehicle
    {
        [Display(Name = "Number of doors")]
        [Range(1,7, ErrorMessage = "You must enter a number of doors between 1 and 5")]
        public int numberOfDoors { get; set; }
        [Display(Name = "Number of wheels")]
        [Range(3, 6, ErrorMessage = "You must enter a number of wheels between 3 and 6")]
        public int numberOfWheels { get; set; }
        [Display(Name = "Body type")]
        [Required(ErrorMessage = "You must select a body type")]
        public BodyType bodyType { get; set; }

        public enum BodyType
        {
            hatchback,
            sedan,
            stationWagon,
            coupe,
            van,
            convertible
        };

        public Car() : base()
        {
            this.vehicleType = VehicleType.Car;
        }
    }
}
