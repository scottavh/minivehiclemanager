﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;

namespace MiniVehicleManager.Models
{
    public abstract class Vehicle
    {
        public int id { get; protected set; }
        [Display(Name = "Make")]
        [Required(ErrorMessage = "You must enter a make")]
        public string make { get; set; }
        [Display(Name = "Model")]
        [Required(ErrorMessage = "You must enter a model")]
        public string model { get; set; }
        [Display(Name = "Engine")]
        public string engine { get; set; }
        public VehicleType vehicleType { get; protected set; }

        private static int _idCounter = 1; // Only needed while this is stored in memory.

        public enum VehicleType { Car }

        // This is only necessary until these are stored in a database, then they can have an autoincrementing ID.
        public static int GetID()
        {
            return _idCounter++;
        }

        public Vehicle()
        {
            this.id = GetID();
        }

        public bool Equals(Vehicle vehicle)
        {
            return this.id == vehicle.id;
        }
    }
}
