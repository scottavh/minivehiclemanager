﻿using MiniVehicleManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiniVehicleManager
{
    public static class VehicleStore
    {
        public static Dictionary<int, Vehicle> vehicles = new Dictionary<int, Vehicle>();

        public static void AddVehicle(Vehicle vehicle)
        {
            vehicles.Add(vehicle.id, vehicle);
        }

        // Although not strictly required, this function is useful during development
        public static Vehicle GetVehicle(int id)
        {
            Vehicle vehicle;
            if (vehicles.TryGetValue(id, out vehicle))
            {
                return vehicle;
            }
            else return null;
        }
    }
}
