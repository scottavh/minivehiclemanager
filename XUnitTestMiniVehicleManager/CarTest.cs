﻿using System;
using Xunit;
using MiniVehicleManager.Models;

namespace XUnitTestMiniVehicleManager
{
    public class CarTest
    {
        [Fact]
        public void CreatedVehiclesHaveUniqueIdTest()
        {
            var car_0 = new Car();
            var car_1 = new Car();
            Assert.NotEqual(car_0.id, car_1.id);
        }
    }
}
