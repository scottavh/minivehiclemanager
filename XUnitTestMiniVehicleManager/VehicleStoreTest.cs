using System;
using Xunit;
using MiniVehicleManager;
using MiniVehicleManager.Models;

namespace XUnitTestMiniVehicleManager
{
    public class VehicleStoreTest
    {
        [Fact]
        public void AddVehicleTest()
        {
            var car = new Car();
            VehicleStore.AddVehicle(car);
            Assert.True(VehicleStore.vehicles.ContainsKey(car.id));
        }

        [Fact]
        public void GetVehicleTest()
        {
            var car = new Car();
            var id = car.id;
            VehicleStore.vehicles.Add(car.id, car);
            Assert.Equal(car, VehicleStore.GetVehicle(id));
        }
    }
}
