﻿using System;
using Xunit;
using MiniVehicleManager.Models;

namespace XUnitTestMiniVehicleManager
{
    public class VehicleTest
    {
        [Fact]
        public void GetIDIncrementsTest()
        {
            var id = Vehicle.GetID();
            Assert.Equal(id + 1, Vehicle.GetID());
        }
    }
}
