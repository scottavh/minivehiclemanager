This is a small MVP miniature vehicle manager.

The only current functionality is a home page with a drop down option redirect to a "Create a car" page.
On that page, a form can be filled out which is validated and submitted to save the data in memory.

The Car class extends a Vehicle class, and the VehicleStore stores Vehicles as opposed to cars so that if other vehicle types are added they can be stored in the same place.

TODOS with a little more time:
- Change the drop downs to ng-select elements
- Store the vehicles in a SQL database, with a vehicle table and a car table that share a vehicleId in a one to one relationship.